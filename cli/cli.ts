import { createInterface as readlineCreateInterface } from "readline";
import { createReadStream, createWriteStream, existsSync } from "fs";
import { Readable, Writable } from "stream";
import { ParsedArgs } from "minimist";
import camelcase from "camelcase";
import {
  BorderCommentBuilder,
  BorderCommentOptionsInterface,
  BorderCommentOptionsKeyValue,
} from "../core";

class BorderCommentBuilderCli {
  private borders: BorderCommentOptionsKeyValue;
  private argv: ParsedArgs;
  private readableStream: Readable = process.stdin;
  private writableStream: Writable = process.stdout;

  private static checkFile = (file: string) => file && existsSync(file);

  private prepareReadableStream(file: string): void {
    if (BorderCommentBuilderCli.checkFile(file)) {
      this.readableStream = createReadStream(file);
    }
  }

  private prepareWritableStream(file: string): void {
    if (BorderCommentBuilderCli.checkFile(file)) {
      this.writableStream = createWriteStream(file);
    }
  }

  public constructor(argv: ParsedArgs, borders: BorderCommentOptionsKeyValue) {
    this.argv = argv;
    this.borders = borders;
    this.prepareReadableStream(this.argv.input);
    this.prepareWritableStream(this.argv.output);
  }

  private get commentOptions(): BorderCommentOptionsInterface {
    const commentType: BorderCommentOptionsInterface = this.borders[
      this.argv.type || "js"
    ];

    const optionsToCheck = [
      "before-border",
      "middle-border",
      "after-border",
      "border-symbol",
      "corner-symbol",
      "side-symbol-left",
      "side-symbol-right",
    ];

    return optionsToCheck
      .filter((item: string) => Boolean(this.argv[item]))
      .reduce(
        (accum: BorderCommentOptionsInterface, current: string) => ({
          ...accum,
          [camelcase(current)]: this.argv[current],
        }),
        commentType
      );
  }

  public perform(): void {
    const messages: Array<string> = [];

    readlineCreateInterface({
      input: this.readableStream,
    })
      .on("line", (d: string) => {
        const correctString = d.toString().split("\t").join("  ");
        messages.push(correctString);
      })
      .on("close", () => {
        this.writableStream.end(
          new BorderCommentBuilder({
            ...this.commentOptions,
            text: messages.join("\n"),
          }).build(),
          () => {}
        );
      });
  }
}

export default BorderCommentBuilderCli;
