#!/usr/bin/env node

/*
 * +------------------------------------------+
 * | Border Comment Builder.                  |
 * | A Node.js CLI app.                       |
 * | (c) 2021, Vladimir Stadnik, t.me/x_doggy |
 * +------------------------------------------+
 */

import minimist from "minimist";
import borders from "./pre-defined-borders.json";
import { BorderCommentBuilderCli } from "./cli";
import { BorderCommentOptionsKeyValue } from "./core";

const cli = new BorderCommentBuilderCli(
  minimist(process.argv.slice(2)),
  borders as BorderCommentOptionsKeyValue
);

cli.perform();
