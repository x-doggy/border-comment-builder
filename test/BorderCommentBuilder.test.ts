import assert from "assert";
import { BorderCommentBuilder } from "../core";

describe("BorderCommentBuilder", function () {
  const defStr: string = "123\n456";

  it("should return correct empty comment", function () {
    const s: string = new BorderCommentBuilder().build();
    assert.equal(
      s,
      `/*
 * +--+
 * |  |
 * +--+
 */
`
    );
  });

  it("should return correct string by default", function () {
    const s: string = new BorderCommentBuilder({ text: defStr }).build();
    assert.equal(
      s,
      `/*
 * +-----+
 * | 123 |
 * | 456 |
 * +-----+
 */
`
    );
  });

  it("should return correct stirng when side symbols defined", function () {
    const s: string = new BorderCommentBuilder({
      text: defStr,
      sideSymbolLeft: "< ",
      sideSymbolRight: " >",
    }).build();
    assert.equal(
      s,
      `/*
 * +-----+
 * < 123 >
 * < 456 >
 * +-----+
 */
`
    );
  });

  it("should return correct stirng when corner symbol defined", function () {
    const s: string = new BorderCommentBuilder({
      text: defStr,
      cornerSymbol: "U",
    }).build();
    assert.equal(
      s,
      `/*
 * U-----U
 * | 123 |
 * | 456 |
 * U-----U
 */
`
    );
  });
});
