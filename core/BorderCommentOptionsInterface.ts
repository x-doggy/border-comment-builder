export interface BorderCommentOptionsInterface {
  beforeBorder?: string;
  middleBorder?: string;
  afterBorder?: string;
  borderSymbol?: string;
  cornerSymbol?: string;
  sideSymbolLeft?: string;
  sideSymbolRight?: string;
  text?: string;
}

export type BorderCommentOptionsKeyValue = {
  [key: string]: BorderCommentOptionsInterface;
};
