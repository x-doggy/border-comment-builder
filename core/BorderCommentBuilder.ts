import { BorderCommentOptionsInterface } from "./BorderCommentOptionsInterface";

class BorderCommentBuilder {
  private beforeBorder: string = "/*";
  private middleBorder: string = " * ";
  private afterBorder: string = " */";
  private borderSymbol: string = "-";
  private cornerSymbol: string = "+";
  private sideSymbolLeft: string = "| ";
  private sideSymbolRight: string = " |";
  private text: string = "";

  public constructor(
    params: BorderCommentOptionsInterface = {
      beforeBorder: "/*",
      middleBorder: " * ",
      afterBorder: " */",
      borderSymbol: "-",
      cornerSymbol: "+",
      sideSymbolLeft: "| ",
      sideSymbolRight: " |",
      text: "",
    }
  ) {
    Object.assign(this, params);
  }

  public build(): string {
    let resultText = "";
    const messages = this.text.split("\n");

    const topBottomBorder = (len: number): string =>
      [
        this.middleBorder,
        this.cornerSymbol,
        Array(len).join(this.borderSymbol),
        this.cornerSymbol,
        "\n",
      ].join("");

    const middleBorder = (line: string): string =>
      [
        this.middleBorder,
        this.sideSymbolLeft,
        line,
        Array(lineMaxLength - line.length - 2).join(" "),
        this.sideSymbolRight,
      ].join("");

    const lineMaxLength =
      messages.reduce((a: string, b: string) => (a.length >= b.length ? a : b))
        .length + 3;

    resultText += this.beforeBorder + "\n";
    resultText += topBottomBorder(lineMaxLength);

    resultText += messages.map(middleBorder).join("\n") + "\n";

    resultText += topBottomBorder(lineMaxLength);
    resultText += this.afterBorder + "\n";

    return resultText;
  }
}

export default BorderCommentBuilder;
