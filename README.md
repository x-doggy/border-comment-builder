# Border comment builder

## What is this?

This is a tool that transforms text you put in `stdin` into a nice border like that:

```js
/*
 * +---------------------+
 * | Nice block comment! |
 * +---------------------+
 */
```

You can insert that thing anywhere in your source code. It would be good to use it in a begin part of your source code.

Written in [Node.js](https://nodejs.org/).

## Launching app

Install app globally:

```
npm i -g border-comment-builder --unsafe-perm=true
```

And after that `border-comment-builder` terminal command will be available.

You can also start app with parameters:

```
border-comment-builder --type python --input comment.txt --output out.txt
```

`echo` and pipe calls help you get result on single line situation:

```
echo "Only single line comment" | border-comment-builder --type twig
```

## JS API usage

You can tweak borders whatever you want.

A `BorderCommentBuilder` constructor gets an object of params and returns compiled string.

![BorderCommentBuilder constructor options](https://gitlab.com/x-doggy/border-comment-builder/raw/master/cli-opts.png)

An example:

```js
const resultString = new BorderCommentBuilder({
  text: "123\n456",
  sideSymbolLeft: "< ",
  sideSymbolRight: " >",
  cornerSymbol: "U",
}).build();
```

Compiled string will be:

```js
/*
 * U-----U
 * < 123 >
 * < 456 >
 * U-----U
 */
```

### Types of comments (predefined)

#### `js` and also by default

```js
/*
 * +-------+
 * | a b c |
 * +-------+
 */
```

#### `c`

```c
  //
  // +-------+
  // | a b c |
  // +-------+
  //
```

#### `pascal`

```
  (*
   * +-------+
   * | a b c |
   * +-------+
   *)
```

#### `python`

```python
  #
  # +-------+
  # | a b c |
  # +-------+
  #
```

#### `html`

```html
<!--
    -- +-------+
    -- | a b c |
    -- +-------+
    -->
```

#### `twig`

```
  {#
   # +-------+
   # | a b c |
   # +-------+
   #}
```

All these borders defined in [pre-defined-borders.json](https://gitlab.com/x-doggy/border-comment-builder/blob/master/pre-defined-borders.json) file.

## CLI util arguments

| argv                       | Description                                          |
| -------------------------- | ---------------------------------------------------- |
| `--type <type_of comment>` | Type of comment. See **"Types of comments"** section |
| `--input <my_comment.txt>` | File containing comment to be processed              |
| `--output <result.txt>`    | File where result comment will be written            |
| `--before-border "(%"`     | `beforeBorder` option                                |
| `--middle-border " % "`    | `middleBorder` option                                |
| `--after-border "%)"`      | `afterBorder` option                                 |
| `--border-symbol "="`      | `borderSymbol` option                                |
| `--corner-symbol "#"`      | `cornerSymbol` option                                |
| `--side-symbol-left "/"`   | `sideSymbolLeft` option                              |
| `--side-symbol-right "\\"` | `sideSymbolRight` option                             |

The text of comment sets **line-by-line** from cli, but also can be read from file.

When you finish writing the text press any of EOF combination (Ctrl+D in Unix-like systems and Ctrl+Z in Win).

## Additional commands

- `npm run build` generates `build` directory.

- `npm run clean` removes `build` directory.

- `npm run test` launches Mocha test utility to check some costructor calls going right.
